<?php

add_filter( 'assesment_3_timezones', 'wpassesment_filter' );
function wpassesment_filter( $text ) {
	$text = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
	return $text;
}

add_filter( 'assesment_3_after_render', 'wpassesment_filter_text' );
function wpassesment_filter_text( $text ) {
	$texts = "Silahkan pilih timezone Anda:";
	return $texts;
}